import cv2
import numpy as np

from collections import deque
import datetime
import argparse
from pprint import PrettyPrinter
import sys

pretty = PrettyPrinter(indent=4).pprint


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument(
    "-v",
    "--video",
    help="path to the (optional) video file; if not set, uses the webcam"
)
ap.add_argument(
    "-mr",
    "--minimum-radius",
    type=int,
    default=10,
    help="minimum movement detection radius, defaults to 10 (int)"
)
ap.add_argument(
    "-en",
    "--experiment-name",
    type=str,
    default="by: Magatti, R. A.",
    help="name of the video window; defaults to the software developer's name (str)"
)
ap.add_argument(
    "-wc",
    "--webcam",
    type=int,
    default=0,
    help="number of the webcam to setup as video stream; defaults to 0 (int)"
)
args = vars(ap.parse_args())

print("Selected options: ")
print(args)

pts = deque(maxlen=5)
if not args.get("video"):
    camera = cv2.VideoCapture(args["webcam"])
else:
    camera = cv2.VideoCapture(args["video"])
fgbg = cv2.createBackgroundSubtractorMOG2()
centers = []


def process_frame():
    ret, frame = camera.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (21, 21), 0)
    res = fgbg.apply(gray)

    kernel = np.ones((5,5), np.uint8)
    #blur = cv2.GaussianBlur(fgmask, (15,15), 0)
    #median = cv2.medianBlur(fgmask, 15)
    #bilateral = cv2.bilateralFilter(fgmask, 15, 75, 75)
    res = cv2.erode(res, None, iterations = 2)
    res = cv2.dilate(res, None, iterations = 2)
    opening = cv2.morphologyEx(res, cv2.MORPH_OPEN, kernel)

    cnts = cv2.findContours(opening.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[-2]
    #closing = cv2.morphologyEx(res, cv2.MORPH_CLOSE, kernel)


    if len(cnts) > 0:
        # find the largest contour in the mask, then use
        # it to compute the minimum enclosing circle and
        # centroid
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        try:
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            centers.append(center)
        except Exception as e:
            print(e.message)


        # only proceed if the radius meets a minimum size
        if radius > args["minimum_radius"]:
            timestamp = datetime.datetime.now().strftime("%d-%m-%Y|%H:%M:%S:%f")
            center_string = str(center[0])+","+str(center[1])
            print("Point: "+center_string, "Timestamp: "+timestamp)
            positions_file.write(center_string+"|")
            positions_file.write(timestamp+"\n")
            try:
                cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
                cv2.circle(frame, center, 5, (0, 0, 255), -1)
                pts.appendleft(center)
            except Exception as e:
                print(e.message)

    cv2.imshow(args["experiment_name"], frame)


if __name__ == '__main__':
    with open("positions_file.txt", "w+") as positions_file:
        while True:
            try:
                key = cv2.waitKey(30) & 0xff
                if key == 27:
                    camera.release()
                    cv2.destroyAllWindows()
                    break
                process_frame()
            except Exception as e:
                print("Error: please check your video feed then restart me.")
                print(e.message)
                if camera and len(centers) > 100:
                    camera.release()
                    cv2.destroyAllWindows()
                    break
