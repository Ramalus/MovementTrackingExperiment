**Project Dependencies**

- Opencv 3.1.0-3
- Python 2.7.12
- opencv-python (pip package)

**Usage:** zerotracking.py [-h] [-v VIDEO] [-mr MINIMUM_RADIUS]
                       [-en EXPERIMENT_NAME] [-wc WEBCAM]

**Examples:**
- ```python zerotracking.py```
- ```python zerotracking.py -v "video_file.mp4"```
- ```python zerotracking.py -wc 1```
- ```python zerotracking.py -en "Window Title"```
- ```python zerotracking.py -mr 5```
- ```python zerotracking.py -en "Nice Name" -mr 2```


**Optional arguments:**

    -h, --help          show this help message and exit
    -v VIDEO, --video VIDEO
                        path to the (optional) video file; if not set, uses
                        the webcam
    -mr MINIMUM_RADIUS, --minimum-radius MINIMUM_RADIUS
                        minimum movement detection radius, defaults to 10(int)
    -en EXPERIMENT_NAME, --experiment-name EXPERIMENT_NAME
                        name of the video window; defaults to the software
                        developer's name (str)
    -wc WEBCAM, --webcam WEBCAM
                        number of the webcam to setup as video stream;
                        defaults to 0 (int)
